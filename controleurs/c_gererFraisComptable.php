<?php
	include("vues/v_sommaire.php");
	$idVisiteur = $_SESSION['idVisiteur'];
	$mois = getMois(date("d/m/Y"));
	$numAnnee =substr( $mois,0,4);
	$numMois =substr( $mois,4,2);
	$action = $_REQUEST['action'];
	switch($action){
	    case 'validerMajFraisForfait':
			$lesFrais = $_REQUEST['lesFrais'];
			if(lesQteFraisValides($lesFrais)){
		  	 	$pdo->majFraisForfait($idVisiteur,$mois,$lesFrais);
			}
			else{
				ajouterErreur("Les valeurs des frais doivent être numériques");
				include("vues/v_erreurs.php");
			}
			break;
		
		case 'supprimerFrais':
			$idFrais = $_REQUEST['idFrais'];
		    $pdo->supprimerFraisHorsForfait($idFrais);
			break;
		
		case'gererListeFiche':
			
			include("vues/v_listesdesVisiteurs.php");
			$leMois = $_REQUEST['lstMois']; 
			$lesMois=$pdo->getLesMoisDisponibles($idVisiteur);
			$moisASelectionner = $leMois;
			include("vues/v_listeMois.php");
		break;
	}
?>